package ua.peha84;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SaveTo(s = "./textContainer.txt")
public class TextContainer {
    String text = """
        On 24 February 2022, Russia invaded Ukraine in a major escalation of the Russo-Ukrainian War,\s
        which began in 2014.  The invasion has resulted in tens of thousands of deaths on both sides.\s
        It has caused  Europe's largest refugee crisis since  World War II. An  estimated  8  million\s
        Ukrainians  were  displaced within their country by late May and 7.8 million fled the country\s
        by 8 November 2022, while Russia, within five weeks of the invasion, experienced its greatest\s
        emigration since the 1917 October Revolution.
        """;

    @Saver
    public void save(String filePath){
        File file = new File(filePath);
        try(var pw = new FileWriter(file, true)){
            pw.write(text);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
