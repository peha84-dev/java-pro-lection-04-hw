package ua.peha84;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class HumanSerializer {
    public static void serialize(Object obj, String path) throws IOException, IllegalAccessException {
        Class<?> cls = Human.class;

        try (OutputStream os = new FileOutputStream(path)) {
            ByteBuffer buffer;
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                if (!field.isAnnotationPresent(Save.class)) {
                    continue;
                }
                int modifier = field.getModifiers();
                if (Modifier.isPrivate(modifier)) {
                    field.setAccessible(true);
                }

                byte[] stringBuffer = ((String) field.get(obj)).getBytes(StandardCharsets.UTF_8);
                buffer = ByteBuffer.allocate(Integer.BYTES);
                buffer.putInt(stringBuffer.length);
                os.write(buffer.array());
                buffer = ByteBuffer.wrap(stringBuffer);

                os.write(buffer.array());
            }
        }
    }

    public static void deserialize(Object obj, String path) throws
            IOException, IllegalAccessException {
        Class<?> cls = Human.class;

        try (InputStream is = new FileInputStream(path)) {
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                if (!field.isAnnotationPresent(Save.class)) {
                    continue;
                }
                int modifier = field.getModifiers();
                if (Modifier.isPrivate(modifier)) {
                    field.setAccessible(true);
                }

                int stringLength = (ByteBuffer.wrap(is.readNBytes(Integer.BYTES))).getInt();
                field.set(obj, new String(is.readNBytes(stringLength)));
            }
        }
    }
}
