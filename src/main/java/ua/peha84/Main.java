package ua.peha84;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws RuntimeException, IOException, IllegalAccessException {
        System.out.println("Hello world!");

        // Задание 1
        System.out.println(testReflection(Summation.class));

        // Задание 2
        Class<?> cls = TextContainer.class;
        Method[] methods = cls.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(Saver.class)) {
                System.out.println("Method: " + method.getName());
                try {
                    SaveTo saveTo = cls.getAnnotation(SaveTo.class);
                    method.invoke(new TextContainer(), saveTo.s());
                    System.out.println("Information saved");
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        // Задание 3
        Human human1 = new Human("H@f$21@!\"\"umanName", "HumanLastname", Gender.MALE);
        String path = "./humanSerialize.txt";
        System.out.println(human1);

        HumanSerializer.serialize(human1, path);

        Human human2 = new Human();
        HumanSerializer.deserialize(human2, path);
        System.out.println(human2);
    }

    public static int testReflection(Class<?> cls) {
        Method[] methods = cls.getDeclaredMethods();
        int res = 0;
        for (Method method : methods) {
            if (method.isAnnotationPresent(Test.class)) {
                Test test = method.getAnnotation(Test.class);
                try {
                    res = (int) method.invoke(null, test.a(), test.b());
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return res;
    }

}